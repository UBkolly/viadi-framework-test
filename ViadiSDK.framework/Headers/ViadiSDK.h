#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class VSDKGridMapArea, VSDKTileFrame, VSDKDirection, VSDKFromToFeatureVector, VSDKDataset, VSDKFromToLabel;

@protocol VSDKTile, VSDKTileFactory;

NS_ASSUME_NONNULL_BEGIN

@interface KotlinBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface KotlinBase (KotlinBaseCopying) <NSCopying>
@end;

__attribute__((objc_runtime_name("KotlinMutableSet")))
__attribute__((swift_name("KotlinMutableSet")))
@interface VSDKMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((objc_runtime_name("KotlinMutableDictionary")))
__attribute__((swift_name("KotlinMutableDictionary")))
@interface VSDKMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((objc_runtime_name("KotlinNumber")))
__attribute__((swift_name("KotlinNumber")))
@interface VSDKNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((objc_runtime_name("KotlinByte")))
__attribute__((swift_name("KotlinByte")))
@interface VSDKByte : VSDKNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((objc_runtime_name("KotlinUByte")))
__attribute__((swift_name("KotlinUByte")))
@interface VSDKUByte : VSDKNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((objc_runtime_name("KotlinShort")))
__attribute__((swift_name("KotlinShort")))
@interface VSDKShort : VSDKNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((objc_runtime_name("KotlinUShort")))
__attribute__((swift_name("KotlinUShort")))
@interface VSDKUShort : VSDKNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((objc_runtime_name("KotlinInt")))
__attribute__((swift_name("KotlinInt")))
@interface VSDKInt : VSDKNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((objc_runtime_name("KotlinUInt")))
__attribute__((swift_name("KotlinUInt")))
@interface VSDKUInt : VSDKNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((objc_runtime_name("KotlinLong")))
__attribute__((swift_name("KotlinLong")))
@interface VSDKLong : VSDKNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((objc_runtime_name("KotlinULong")))
__attribute__((swift_name("KotlinULong")))
@interface VSDKULong : VSDKNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((objc_runtime_name("KotlinFloat")))
__attribute__((swift_name("KotlinFloat")))
@interface VSDKFloat : VSDKNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((objc_runtime_name("KotlinDouble")))
__attribute__((swift_name("KotlinDouble")))
@interface VSDKDouble : VSDKNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((objc_runtime_name("KotlinBoolean")))
__attribute__((swift_name("KotlinBoolean")))
@interface VSDKBoolean : VSDKNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GridMap")))
@interface VSDKGridMap : KotlinBase
- (instancetype)initWithNumberOfColumns:(int32_t)numberOfColumns numberOfRows:(int32_t)numberOfRows __attribute__((swift_name("init(numberOfColumns:numberOfRows:)"))) __attribute__((objc_designated_initializer));
- (NSArray<id> *)getValues __attribute__((swift_name("getValues()")));
- (BOOL)isInsideRow:(int32_t)row column:(int32_t)column rowSpan:(int32_t)rowSpan columnSpan:(int32_t)columnSpan __attribute__((swift_name("isInside(row:column:rowSpan:columnSpan:)")));
- (void)putRow:(int32_t)row column:(int32_t)column rowSpan:(int32_t)rowSpan columnSpan:(int32_t)columnSpan value:(id _Nullable)value __attribute__((swift_name("put(row:column:rowSpan:columnSpan:value:)")));
- (id _Nullable)getRow:(int32_t)row column:(int32_t)column __attribute__((swift_name("get(row:column:)")));
- (NSArray<id> *)getRow:(int32_t)row column:(int32_t)column rowSpan:(int32_t)rowSpan columnSpan:(int32_t)columnSpan __attribute__((swift_name("get(row:column:rowSpan:columnSpan:)")));
- (void)removeValue:(id _Nullable)value __attribute__((swift_name("remove(value:)")));
- (id _Nullable)removeRow:(int32_t)row column:(int32_t)column __attribute__((swift_name("remove(row:column:)")));
- (NSArray<id> *)removeRow:(int32_t)row column:(int32_t)column rowSpan:(int32_t)rowSpan columnSpan:(int32_t)columnSpan __attribute__((swift_name("remove(row:column:rowSpan:columnSpan:)")));
- (void)clear __attribute__((swift_name("clear()")));
- (NSDictionary<VSDKInt *, NSArray<VSDKGridMapArea *> *> *)getEmptyColumnsPerRowMaximumColumnSpan:(int32_t)maximumColumnSpan __attribute__((swift_name("getEmptyColumnsPerRow(maximumColumnSpan:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GridMapArea")))
@interface VSDKGridMapArea : KotlinBase
- (instancetype)initWithRow:(int32_t)row column:(int32_t)column rowSpan:(int32_t)rowSpan columnSpan:(int32_t)columnSpan __attribute__((swift_name("init(row:column:rowSpan:columnSpan:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t row __attribute__((swift_name("row")));
@property (readonly) int32_t column __attribute__((swift_name("column")));
@property (readonly) int32_t rowSpan __attribute__((swift_name("rowSpan")));
@property (readonly) int32_t columnSpan __attribute__((swift_name("columnSpan")));
@end;

__attribute__((swift_name("TileGrid")))
@interface VSDKTileGrid : KotlinBase
- (instancetype)initWithNumberOfColumns:(int32_t)numberOfColumns numberOfRows:(int32_t)numberOfRows __attribute__((swift_name("init(numberOfColumns:numberOfRows:)"))) __attribute__((objc_designated_initializer));
- (void)setTilesTiles:(id)tiles __attribute__((swift_name("setTiles(tiles:)")));
- (void)addTileTile:(VSDKTileFrame *)tile __attribute__((swift_name("addTile(tile:)")));
- (void)removeTileTile:(VSDKTileFrame *)tile __attribute__((swift_name("removeTile(tile:)")));
- (NSArray<VSDKDirection *> *)getResizeOptionsTile:(VSDKTileFrame *)tile __attribute__((swift_name("getResizeOptions(tile:)")));
- (BOOL)canResizeTileFromTileState:(VSDKTileFrame *)fromTileState direction:(VSDKDirection *)direction __attribute__((swift_name("canResizeTile(fromTileState:direction:)")));
- (BOOL)canEnlargeTileFromTileState:(VSDKTileFrame *)fromTileState direction:(VSDKDirection *)direction __attribute__((swift_name("canEnlargeTile(fromTileState:direction:)")));
- (BOOL)dragTileToFromTileState:(VSDKTileFrame *)fromTileState toColumn:(float)toColumn toRow:(float)toRow __attribute__((swift_name("dragTileTo(fromTileState:toColumn:toRow:)")));
- (BOOL)dragTileToFromTileState:(VSDKTileFrame *)fromTileState toColumn:(float)toColumn toRow:(float)toRow newColumnSpan:(float)newColumnSpan newRowSpan:(float)newRowSpan __attribute__((swift_name("dragTileTo(fromTileState:toColumn:toRow:newColumnSpan:newRowSpan:)")));
- (VSDKTileFrame * _Nullable)getTileAtColumn:(int32_t)column row:(int32_t)row __attribute__((swift_name("getTileAt(column:row:)")));
- (VSDKTileFrame * _Nullable)findTileTile:(id<VSDKTile>)tile __attribute__((swift_name("findTile(tile:)")));
- (NSDictionary<VSDKInt *, NSArray<VSDKGridMapArea *> *> *)getEmptySpacesPerRowMaximumColumnSpan:(int32_t)maximumColumnSpan __attribute__((swift_name("getEmptySpacesPerRow(maximumColumnSpan:)")));
@property BOOL isEditing __attribute__((swift_name("isEditing")));
@property (readonly) NSArray<VSDKTileFrame *> *tiles __attribute__((swift_name("tiles")));
@property (readonly) int32_t numberOfColumns __attribute__((swift_name("numberOfColumns")));
@property (readonly) int32_t numberOfRows __attribute__((swift_name("numberOfRows")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SeparatorTileGrid")))
@interface VSDKSeparatorTileGrid : VSDKTileGrid
- (instancetype)initWithNumberOfColumns:(int32_t)numberOfColumns numberOfRows:(int32_t)numberOfRows __attribute__((swift_name("init(numberOfColumns:numberOfRows:)"))) __attribute__((objc_designated_initializer));
- (void)setTilesTiles:(id)tiles __attribute__((swift_name("setTiles(tiles:)")));
- (void)addTileTile:(VSDKTileFrame *)tile __attribute__((swift_name("addTile(tile:)")));
- (int32_t)initialSeparatorPosition __attribute__((swift_name("initialSeparatorPosition()")));
- (int32_t)getMinimumSeparatorRowIndex __attribute__((swift_name("getMinimumSeparatorRowIndex()")));
- (int32_t)getMaximumSeparatorRowIndex __attribute__((swift_name("getMaximumSeparatorRowIndex()")));
- (int32_t)findFirstRowWithTiles __attribute__((swift_name("findFirstRowWithTiles()")));
- (int32_t)snapSeparatorToNearesValidRowIndexDesiredRowIndex:(float)desiredRowIndex __attribute__((swift_name("snapSeparatorToNearesValidRowIndex(desiredRowIndex:)")));
@property (readonly) int32_t currentSeparatorRowIndex __attribute__((swift_name("currentSeparatorRowIndex")));
@end;

__attribute__((swift_name("Tile")))
@protocol VSDKTile
@required
- (BOOL)isMovable __attribute__((swift_name("isMovable()")));
- (BOOL)isEqualTile:(id<VSDKTile>)tile __attribute__((swift_name("isEqual(tile:)")));
- (NSString *)desc __attribute__((swift_name("desc()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TileFrame")))
@interface VSDKTileFrame : KotlinBase
- (instancetype)initWithTile:(id<VSDKTile>)tile column:(int32_t)column row:(int32_t)row columnSpan:(int32_t)columnSpan rowSpan:(int32_t)rowSpan __attribute__((swift_name("init(tile:column:row:columnSpan:rowSpan:)"))) __attribute__((objc_designated_initializer));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSString *)desc __attribute__((swift_name("desc()")));
@property (readonly) id<VSDKTile> tile __attribute__((swift_name("tile")));
@property (readonly) int32_t column __attribute__((swift_name("column")));
@property (readonly) int32_t row __attribute__((swift_name("row")));
@property (readonly) int32_t columnSpan __attribute__((swift_name("columnSpan")));
@property (readonly) int32_t rowSpan __attribute__((swift_name("rowSpan")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Coordinate")))
@interface VSDKCoordinate : KotlinBase
- (instancetype)initWithLongitude:(double)longitude latitude:(double)latitude __attribute__((swift_name("init(longitude:latitude:)"))) __attribute__((objc_designated_initializer));
@property (readonly) double longitude __attribute__((swift_name("longitude")));
@property (readonly) double latitude __attribute__((swift_name("latitude")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol VSDKKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface VSDKKotlinEnum : KotlinBase <VSDKKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(VSDKKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Direction")))
@interface VSDKDirection : VSDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
@property (class, readonly) VSDKDirection *toLeft __attribute__((swift_name("toLeft")));
@property (class, readonly) VSDKDirection *up __attribute__((swift_name("up")));
@property (class, readonly) VSDKDirection *toRight __attribute__((swift_name("toRight")));
@property (class, readonly) VSDKDirection *down __attribute__((swift_name("down")));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (int32_t)compareToOther:(VSDKDirection *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) BOOL isVertical __attribute__((swift_name("isVertical")));
@property (readonly) BOOL isHorizontal __attribute__((swift_name("isHorizontal")));
@end;

__attribute__((swift_name("DynamicTilePositioner")))
@protocol VSDKDynamicTilePositioner
@required
- (int32_t)maxDisplayableTiles __attribute__((swift_name("maxDisplayableTiles()")));
- (NSArray<VSDKTileFrame *> *)placeTilesTiles:(NSArray<id<VSDKTile>> *)tiles __attribute__((swift_name("placeTiles(tiles:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DefaultDynamicTilePositioner")))
@interface VSDKDefaultDynamicTilePositioner : KotlinBase <VSDKDynamicTilePositioner>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)setDynamicAreaX:(int32_t)x y:(int32_t)y width:(int32_t)width height:(int32_t)height __attribute__((swift_name("setDynamicArea(x:y:width:height:)")));
- (int32_t)maxDisplayableTiles __attribute__((swift_name("maxDisplayableTiles()")));
- (void)setLastKnownTilesLastKnownTiles:(id)lastKnownTiles __attribute__((swift_name("setLastKnownTiles(lastKnownTiles:)")));
- (NSArray<VSDKTileFrame *> *)placeTilesTiles:(NSArray<id<VSDKTile>> *)tiles __attribute__((swift_name("placeTiles(tiles:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DoubleColumnTilePositioner")))
@interface VSDKDoubleColumnTilePositioner : KotlinBase <VSDKDynamicTilePositioner>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)setDynamicAreaNumberOfDoubleColumns:(int32_t)numberOfDoubleColumns numberOfRows:(int32_t)numberOfRows startColumn:(int32_t)startColumn startRow:(int32_t)startRow __attribute__((swift_name("setDynamicArea(numberOfDoubleColumns:numberOfRows:startColumn:startRow:)")));
- (int32_t)maxDisplayableTiles __attribute__((swift_name("maxDisplayableTiles()")));
- (NSArray<VSDKTileFrame *> *)placeTilesTiles:(NSArray<id<VSDKTile>> *)tiles __attribute__((swift_name("placeTiles(tiles:)")));
@end;

__attribute__((swift_name("DynamicDataRecord")))
@protocol VSDKDynamicDataRecord
@required
@end;

__attribute__((swift_name("DynamicTileGenerator")))
@interface VSDKDynamicTileGenerator : KotlinBase
- (instancetype)initWithTileFactory:(id<VSDKTileFactory>)tileFactory __attribute__((swift_name("init(tileFactory:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<VSDKTileFactory> tileFactory __attribute__((swift_name("tileFactory")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KnnDynamicDataRecord")))
@interface VSDKKnnDynamicDataRecord : KotlinBase <VSDKDynamicDataRecord>
- (instancetype)initWithLocationID:(NSString *)locationID __attribute__((swift_name("init(locationID:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *locationID __attribute__((swift_name("locationID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KnnDynamicTileGenerator")))
@interface VSDKKnnDynamicTileGenerator : VSDKDynamicTileGenerator
- (instancetype)initWithTileFactory:(id<VSDKTileFactory>)tileFactory __attribute__((swift_name("init(tileFactory:)"))) __attribute__((objc_designated_initializer));
- (NSArray<id<VSDKTile>> *)generateTilesMaxTiles:(int32_t)maxTiles featureVector:(VSDKFromToFeatureVector *)featureVector __attribute__((swift_name("generateTiles(maxTiles:featureVector:)")));
- (void)setDatasetDataset:(VSDKDataset *)dataset __attribute__((swift_name("setDataset(dataset:)")));
@end;

__attribute__((swift_name("TileFactory")))
@protocol VSDKTileFactory
@required
- (id<VSDKTile>)createTileData:(id<VSDKDynamicDataRecord>)data __attribute__((swift_name("createTile(data:)")));
@end;

__attribute__((swift_name("Distance")))
@protocol VSDKDistance
@required
- (double)getDistanceFeatureVector1:(VSDKFromToFeatureVector *)featureVector1 featureVector2:(VSDKFromToFeatureVector *)featureVector2 __attribute__((swift_name("getDistance(featureVector1:featureVector2:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FromToFeatureDistance")))
@interface VSDKFromToFeatureDistance : KotlinBase <VSDKDistance>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (double)getDistanceFeatureVector1:(VSDKFromToFeatureVector *)featureVector1 featureVector2:(VSDKFromToFeatureVector *)featureVector2 __attribute__((swift_name("getDistance(featureVector1:featureVector2:)")));
- (double)getEucleadianDistanceFeatureDistances:(NSArray<VSDKDouble *> *)featureDistances __attribute__((swift_name("getEucleadianDistance(featureDistances:)")));
@property (readonly) double locationDistanceInKm __attribute__((swift_name("locationDistanceInKm")));
@end;

__attribute__((swift_name("Classifier")))
@interface VSDKClassifier : KotlinBase
- (instancetype)initWithDistance:(id<VSDKDistance>)distance __attribute__((swift_name("init(distance:)"))) __attribute__((objc_designated_initializer));
- (void)fitDataset:(VSDKDataset *)dataset __attribute__((swift_name("fit(dataset:)")));
- (void)fitFeatures:(VSDKFromToFeatureVector *)features label:(VSDKFromToLabel *)label __attribute__((swift_name("fit(features:label:)")));
- (double)accuracyDataset:(VSDKDataset *)dataset k:(int32_t)k __attribute__((swift_name("accuracy(dataset:k:)")));
- (VSDKFromToLabel * _Nullable)classifyFeatures:(VSDKFromToFeatureVector *)features k:(int32_t)k __attribute__((swift_name("classify(features:k:)")));
- (id _Nullable)modeList:(NSArray<id> *)list __attribute__((swift_name("mode(list:)")));
@property (readonly) id<VSDKDistance> distance __attribute__((swift_name("distance")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BruteForceClassifier")))
@interface VSDKBruteForceClassifier : VSDKClassifier
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithDistance:(id<VSDKDistance>)distance __attribute__((swift_name("init(distance:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)fitFeatures:(VSDKFromToFeatureVector *)features label:(VSDKFromToLabel *)label __attribute__((swift_name("fit(features:label:)")));
- (VSDKFromToLabel * _Nullable)classifyFeatures:(VSDKFromToFeatureVector *)features k:(int32_t)k __attribute__((swift_name("classify(features:k:)")));
- (NSArray<VSDKFromToLabel *> *)getKNearestNeighborsFeatures:(VSDKFromToFeatureVector *)features k:(int32_t)k __attribute__((swift_name("getKNearestNeighbors(features:k:)")));
- (VSDKMutableDictionary<VSDKDouble *, NSMutableArray<VSDKFromToLabel *> *> *)distanceLabelMapFeatures:(VSDKFromToFeatureVector *)features __attribute__((swift_name("distanceLabelMap(features:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Calendar")))
@interface VSDKCalendar : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithTimeInMillisecondsSince1970:(int64_t)timeInMillisecondsSince1970 timezone:(NSString *)timezone __attribute__((swift_name("init(timeInMillisecondsSince1970:timezone:)"))) __attribute__((objc_designated_initializer));
- (int64_t)timeInDaysSince1970 __attribute__((swift_name("timeInDaysSince1970()")));
- (int32_t)timeOfDay __attribute__((swift_name("timeOfDay()")));
- (int32_t)dayOfWeek __attribute__((swift_name("dayOfWeek()")));
- (int32_t)dayOfMonth __attribute__((swift_name("dayOfMonth()")));
- (int32_t)monthOfYear __attribute__((swift_name("monthOfYear()")));
@end;

__attribute__((swift_name("Dataset")))
@interface VSDKDataset : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addEntryFeatures:(VSDKFromToFeatureVector *)features fromToLabel:(VSDKFromToLabel *)fromToLabel __attribute__((swift_name("addEntry(features:fromToLabel:)")));
@property (readonly) NSMutableArray<VSDKFromToFeatureVector *> *featuresList __attribute__((swift_name("featuresList")));
@property (readonly) NSMutableArray<VSDKFromToLabel *> *labelsList __attribute__((swift_name("labelsList")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FromToFeatureVector")))
@interface VSDKFromToFeatureVector : KotlinBase
- (instancetype)initWithCalendar:(VSDKCalendar *)calendar lat:(double)lat lon:(double)lon __attribute__((swift_name("init(calendar:lat:lon:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCalendar:(VSDKCalendar *)calendar __attribute__((swift_name("init(calendar:)"))) __attribute__((objc_designated_initializer));
- (int64_t)timeInDaysSince1970 __attribute__((swift_name("timeInDaysSince1970()")));
- (VSDKCoordinate *)coordinate __attribute__((swift_name("coordinate()")));
- (int32_t)timeOfDay __attribute__((swift_name("timeOfDay()")));
- (int32_t)dayOfWeek __attribute__((swift_name("dayOfWeek()")));
- (int32_t)dayOfMonth __attribute__((swift_name("dayOfMonth()")));
- (int32_t)monthOfYear __attribute__((swift_name("monthOfYear()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FromToLabel")))
@interface VSDKFromToLabel : KotlinBase
- (instancetype)initWithFrom:(NSString * _Nullable)from to:(NSString * _Nullable)to __attribute__((swift_name("init(from:to:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString * _Nullable from __attribute__((swift_name("from")));
@property (readonly) NSString * _Nullable to __attribute__((swift_name("to")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CsvDataset")))
@interface VSDKCsvDataset : VSDKDataset
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SqlDataset")))
@interface VSDKSqlDataset : VSDKDataset
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

NS_ASSUME_NONNULL_END
